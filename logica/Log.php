<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogDAO.php";
class Log{
    private $idLog;
    private $Autor;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $Datos;
    private $conexion;
    private $LogDAO;
    
    public function getIdLog(){
        return $this -> idLog;
    }
    
    public function getAutor(){
        return $this -> Autor;
    }
    
    public function getAccion(){
        return $this -> Accion;
    }
    
    public function getFecha(){
        return $this -> Fecha;
    }
    
    public function getHora(){
        return $this -> Hora;
    }
    public function getDatos(){
        return $this -> Datos;
    }
    
    
    public function Log($idLog = "", $Autor = "", $Accion = "", $Datos= "", $Fecha = "", $Hora = ""){
        $this -> idLog = $idLog;
        $this -> Autor = $Autor;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> Datos = $Datos;
        $this -> conexion = new Conexion();
        $this -> LogDAO = new LogDAO($this -> idLog ,$this -> Autor ,$this -> Accion ,$this -> Datos,$this -> Fecha  ,$this -> Hora);
    }
    
 

    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultar());
        $this -> conexion -> cerrar();
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarFiltro($filtro));
        $log = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($log, $l);
        }
        $this -> conexion -> cerrar();
        return $log;
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarPaginacion($cantidad, $pagina));
        $log= array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5]);
            array_push($log, $l);
        }
        $this -> conexion -> cerrar();
        return $log;
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>