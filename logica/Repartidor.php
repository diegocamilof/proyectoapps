<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/RepartidorDAO.php";
class Repartidor{
    private $idRepartidor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    private $conexion;
    private $repartidorDAO;
    
    public function getIdRepartidor(){
        return $this -> idRepartidor;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getApellido(){
        return $this -> apellido;
    }
    
    public function getCorreo(){
        return $this -> correo;
    }
    
    public function getClave(){
        return $this -> clave;
    }
    
    public function getFoto(){
        return $this -> foto;
    }
    
    public function getEstado(){
        return $this -> estado;
    }
    
    public function Repartidor($idRepartidor = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idRepartidor = $idRepartidor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
        $this -> conexion = new Conexion();
        $this -> repartidorDAO = new RepartidorDAO($this -> idRepartidor, $this -> nombre, $this -> apellido, $this -> correo, $this -> clave, $this -> foto, $this -> estado);
    }
    
    public function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> existeCorreo());
        $this -> conexion -> cerrar();
        return $this -> conexion -> numFilas();
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idRepartidor = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    
    
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> consultarPaginacion($cantidad, $pagina));
        $repartidor= array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r = new Repartidor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4], $resultado[5]);
            array_push($repartidor, $r);
        }
        $this -> conexion -> cerrar();
        return $repartidor;
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> consultarTodos());
        $repartidor = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r = new Repartidor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4], $resultado[5]);
            array_push($repartidor, $r);
        }
        $this -> conexion -> cerrar();
        return $repartidor;
    }
    
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> repartidorDAO -> consultarFiltro($filtro));
        $repartidor = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $r = new Repartidor($resultado[0], $resultado[1], $resultado[2], $resultado[3],"", $resultado[4], $resultado[5]);
            array_push($repartidor, $r);
        }
        $this -> conexion -> cerrar();
        return $repartidor;
    }
    
    
    
    
    
    
}

?>