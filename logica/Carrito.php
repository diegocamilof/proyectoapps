<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CarritoDAO.php";
class Carrito{
    private $idCarrito;
    private $fecha;
    private $valorFinal;
    private $idCliente;    
    private $idRepartidor;
    private $conexion;
    private $productoCarritoDAO;
  
    public function getIdCarrito()
    {
        return $this->idCarrito;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }

    public function getIdRepartidor()
    {
        return $this->idRepartidor;
    }

    public function Carrito($idCarrito = "", $Fecha = "",$valorFinal = "", $idCliente = "", $idRepartidor = ""){
        $this -> idCarrito = $idCarrito;
        $this -> fecha = $Fecha;
        $this -> valorFinal=$valorFinal;
        $this -> idCliente = $idCliente;
        $this -> idRepartidor = $idRepartidor;
        $this -> conexion = new Conexion();
        $this -> productoCarritoDAO = new CarritoDAO($this -> idCarrito, $this -> fecha, $this -> valorFinal,$this -> idCliente, $this -> idRepartidor);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> fecha = $resultado[0];
        $this -> valorFinal = $resultado[1];
        $this -> idCliente = $resultado[2];
        $this -> idRepartidor = $resultado[3];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Carrito($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }
    
    //     public function consultarPaginacion($cantidad, $pagina){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarPaginacion($cantidad, $pagina));
    //         $productos = array();
    //         while(($resultado = $this -> conexion -> extraer()) != null){
    //             $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
    //             array_push($productos, $p);
    //         }
    //         $this -> conexion -> cerrar();
    //         return $productos;
    //     }
    
    //     public function consultarCantidad(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarCantidad());
    //         $this -> conexion -> cerrar();
    //         return $this -> conexion -> extraer()[0];
    //     }
    
    //     public function editar(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> editar());
    //         $this -> conexion -> cerrar();
    //     }
    
}

?>