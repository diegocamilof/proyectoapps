<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoCarritoDAO.php";
class ProductoCarrito{
    private $idProductoCarrito;
    private $cantidad;
    private $precio;
    private $idProducto;
    private $idCarrito;
    private $conexion;
    private $idProductoAux;
    private $cantidadAux;
    private $arrayid;
    private $arraycant;
    private $productoCarritoDAO;

    
   
    public function getArrayid()
    {
        return $this->arrayid;
    }

    public function getArraycant()
    {
        return $this->arraycant;
    }

    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

   
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;
    }

    public function getIdProductoCarrito()
    {
        return $this->idProductoCarrito;
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getIdProducto()
    {
        return $this->idProducto;
    }

    public function getIdCarrito()
    {
        return $this->idCarrito;
    }
   
     
   

    public function ProductoCarrito($idProductoCarrito = "", $cantidad = "", $precio = "", $idProducto = "", $idCarrito = ""){
        $this -> idProductoCarrito = $idProductoCarrito;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> idProducto = $idProducto;
        $this -> idCarrito = $cantidad;
        $this -> arrayid = new ArrayObject();
        $this -> arraycant = new ArrayObject();
        $this -> conexion = new Conexion();
        $this -> productoCarritoDAO = new ProductoCarritoDAO($this -> idProducto, $this -> cantidad, $this -> precio,$this -> idProducto, $this -> idCarrito);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> imagen = $resultado[3];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarTodos());
        $productos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($productos, $p);
        }
        $this -> conexion -> cerrar();
        return $productos;
    }
    
//     public function consultarPaginacion($cantidad, $pagina){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarPaginacion($cantidad, $pagina));
//         $productos = array();
//         while(($resultado = $this -> conexion -> extraer()) != null){
//             $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
//             array_push($productos, $p);
//         }
//         $this -> conexion -> cerrar();
//         return $productos;
//     }
    
//     public function consultarCantidad(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> consultarCantidad());
//         $this -> conexion -> cerrar();
//         return $this -> conexion -> extraer()[0];
//     }
    
//     public function editar(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> productoCarritoDAO -> editar());
//         $this -> conexion -> cerrar();
//     }
    public function agregarArray(){
        array_push($this -> arrayid,$this -> idProducto);
        array_push($this -> arraycant,$this -> cantidad);
        
    }
}

?>