<?php
class RepartidorDAO{
    private $idRepartidor;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;
    
    public function RepartidorDAO($idRepartidor = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idRepartidor = $idRepartidor;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;
    }
    
    public function existeCorreo(){
        return "select correo
                from repartidor
                where correo = '" . $this -> correo .  "'";
    }
    
    public function registrar(){
        return "insert into repartidor (nombre,apellido,correo, clave, estado)
                values ('" . $this -> nombre . "','" . $this -> apellido . "','" . $this -> correo . "', '" . md5($this -> clave) . "', '1')";
    }
    
   
    
    public function activar(){
        return "update repartidor
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }
    
    public function autenticar(){
        return "select Id, estado
                from repartidor
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }
    
    public function consultar(){
        return"select nombre, apellido, correo, foto
                from repartidor
                where Id = '" . $this -> idRepartidor .  "'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select Id, nombre, apellido, correo,foto,estado
                from repartidor
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    public function consultarCantidad(){
        return "select count(Id)
                from repartidor";
    }
    public function consultarTodos(){
        return "select Id, nombre, apellido, correo,foto,estado
                from repartidor";
    }
    public function cambiarEstado(){
        return "update repartidor
                set estado = '" . $this -> estado . "'
                where Id = '" . $this -> idRepartidor .  "'";
    }
    public function consultarFiltro($filtro){
        return"select  Id, nombre, apellido, correo,foto,estado
                from repartidor
                where nombre like '%" . $filtro . "%' or apellido like '%" . $filtro . "%' or correo like '%" . $filtro . "%'";
    }
    
    
}

?>