<?php
class ProductoDAO{
    private $idProducto;
    private $nombre;
    private $cantidad;
    private $precio;
    private $imagen;
       
    public function ProductoDAO($idProducto = "", $nombre = "", $cantidad = "", $precio = "", $imagen = ""){
        $this -> idProducto = $idProducto;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio;
        $this -> imagen = $imagen;
    }

    public function consultar(){
        return "select nombre, cantidad, precio ,imagen
                from producto
                where Id = '" . $this -> idProducto .  "'";
    }    
    
    public function insertar(){
        return "insert into producto (nombre, cantidad, precio, imagen)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> precio . "','" . $this -> imagen . "')";
    }
    
    public function consultarTodos(){
        return "select Id, nombre, cantidad, precio,imagen
                from Producto";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select Id, nombre, cantidad, precio,imagen
                from producto
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(Id)
                from Producto";
    }
 
    public function editar(){
        return "update Producto
                set nombre = '" . $this -> nombre . "', cantidad = '" . $this -> cantidad . "', precio = " . $this -> precio . ", Imagen = '". $this -> imagen."'
                where Id = '" . $this -> idProducto .  "'";
    }
    public function consultarFiltro($filtro){
        return "select id, nombre, cantidad, precio
                from Producto
                where nombre like '%" . $filtro . "%' or cantidad like '" . $filtro . "%' or precio like '" . $filtro . "%'";
    }
    
}

?>