<?php
class ClienteDAO{
    private $idCliente;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $foto;
    private $estado;

    public function ClienteDAO($idCliente = "", $nombre = "", $apellido = "", $correo = "", $clave = "", $foto = "", $estado = ""){
        $this -> idCliente = $idCliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> estado = $estado;        
    }

    public function existeCorreo(){
        return "select correo
                from Cliente
                where correo = '" . $this -> correo .  "'";
    }
        
    public function registrar(){
        return "insert into Cliente (correo, clave, estado)
                values ('" . $this -> correo . "', '" . md5($this -> clave) . "', '1')";
    }
 
    public function verificarCodigoActivacion($codigoActivacion){
        return "select Id
                from cliente
                where correo = '" . $this -> correo .  "' and codigoActivacion = '" . md5($codigoActivacion) . "'";
    }
    
    public function activar(){
        return "update cliente
                set estado = '1'
                where correo = '" . $this -> correo .  "'";
    }
    
    public function autenticar(){
        return "select Id, estado
                from cliente
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select nombre, apellido, correo, foto
                from cliente
                where Id = '" . $this -> idCliente .  "'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select Id, nombre, apellido, correo,foto,estado
                from cliente
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    public function consultarCantidad(){
        return "select count(Id)
                from cliente";
    }
    public function consultarTodos(){
        return "select id, nombre, apellido, correo, estado
                from cliente";
    }
    public function cambiarEstado(){
        return "update Cliente
                set estado = '" . $this -> estado . "'
                where Id = '" . $this -> idCliente .  "'";
    }
    public function consultarFiltro($filtro){
        return"select  Id, nombre, apellido, correo,foto,estado
                from cliente
                where nombre like '%" . $filtro . "%' or apellido like '%" . $filtro . "%' or correo like '%" . $filtro . "%'";
    }
    
}

?>