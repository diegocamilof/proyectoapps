<?php
class LogDAO{
    private $idLog;
    private $Autor;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $Datos;
    private $conexion;
    private $LogDAO;
    
  
    
    public function LogDAO($idLog = "", $Autor = "", $Accion = "", $Datos= "", $Fecha = "", $Hora = ""){
        $this -> idLog = $idLog;
        $this -> Autor = $Autor;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> Datos = $Datos;
        $this -> conexion = new Conexion();
    }
    
    
    
    
    public function insertar(){
        return "insert into Log(Id,Autor,Accion,Datos,Fecha,Hora)
                values ('". $this -> idLog ."','". $this -> Autor ."','". $this -> Accion ."','". $this -> Datos ."','". $this -> Fecha ."','". $this -> Hora ."')";
        
    }
    public function consultar(){
        return "Select Id,Autor,Accion,Datos,Fecha,Hora
                From Log";
        
    }
    public function consultarFiltro($filtro){
        return "select  Id, autor, accion, Datos,fecha,hora
                from log
                where autor like '%" . $filtro . "%' or accion like '%" . $filtro . "%'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select Id, autor, accion, Datos,fecha,hora
                from log
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    public function consultarCantidad(){
        return "select count(Id)
                from log";
    }
}