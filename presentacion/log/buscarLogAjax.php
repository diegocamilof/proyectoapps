<?php
$filtro = $_GET["filtro"];
$log= new Log();
$logs = $log -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Consultar Log</h4>
				</div>
				<div class="text-right"><?php echo count($logs) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
							<tr>
							<th>#</th>
							<th>Autor</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($logs as $logActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $logActual -> getAutor() . "</td>";
						    echo "<td>" . $logActual -> getAccion() . "</td>";
						    echo "<td>" . $logActual -> getDatos() . "</td>";
						    echo "<td>" . $logActual -> getFecha() . "</td>";
						    echo "<td>" . $logActual -> getHora() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					
				</div>
            </div>
		</div>
	</div>
</div>

