<?php
$administrador= new Administrador($_SESSION["id"]);
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$apellido = "";
if(isset($_POST["apellido"])){
    $apellido = $_POST["apellido"];
}
$correo = "";
if(isset($_POST["correo"])){
    $correo = $_POST["correo"];
}
$clave = "";
if(isset($_POST["clave"])){
    $clave = $_POST["clave"];
}

if(isset($_POST["crear"])){
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "img/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        
        $repartidor = new Repartidor("",$nombre, $apellido, $correo,$clave, $rutaRemota);
        $repartidor -> registrar();
    }else{
        $repartidor = new Repartidor("",$nombre, $apellido, $correo,$clave);
        $repartidor -> registrar();
    }
    $administrador -> consultar();
    $autor=$_SESSION["rol"].": ".$administrador -> getNombre()." ". $administrador -> getApellido();
    $datos= $nombre." ".$apellido;
    $log =new Log("",$autor,"Creacion de Repartidor",$datos,date("Y-m-d"),date("H:i:s"));
    $log -> insertar();
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Crear Repartidor</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos creados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/repartidor/crearRepartidor.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Correo</label> 
							<input type="text" name="correo" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Clave</label> 
							<input type="text" name="clave" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Imagen</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						
						<button type="submit" name="crear" class="btn btn-warning">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>




						