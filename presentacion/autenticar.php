<?php

$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$cliente = new Cliente("", "", "", $correo, $clave);
$repartidor = new Repartidor("","","",$correo,$clave);

$Accion="Inicio de sesion";


    if($administrador -> autenticar()){
        $_SESSION["id"] = $administrador -> getIdAdministrador();
        $_SESSION["rol"] = "Administrador";
        $administrador = new Administrador($_SESSION["id"]);
        $administrador -> consultar();
        $Datos= $administrador -> getNombre(). " " . $administrador -> getApellido();
        $log= new Log("","Administrador: ".$Datos,$Accion,"Sin datos",date("Y-m-d"),date("H:i:s"));
        $log -> insertar();
        header("Location: index.php?pid=" . base64_encode("presentacion/sesionAdministrador.php"));
    }else if($cliente -> autenticar()){
        if($cliente -> getEstado() == 0){
            header("Location: index.php?error=3");
        }else{
            $_SESSION["id"] = $cliente -> getIdCliente();
            $_SESSION["rol"] = "Cliente";
            $cliente = new Cliente($_SESSION["id"]);
            $cliente -> consultar();
            $Datos= $cliente -> getNombre(). " " . $cliente -> getApellido();
            $log= new Log("","Cliente: ".$Datos,$Accion,"Sin datos",date("Y-m-d"),date("H:i:s"));
            $log -> insertar();
            header("Location: index.php?pid=" . base64_encode("presentacion/sesionCliente.php"));
        }
        }else if($repartidor -> autenticar()){
            if($repartidor -> getEstado() == 0){
                header("Location: index.php?error=3");
            }else{
                $_SESSION["id"] = $repartidor -> getIdRepartidor();
                $_SESSION["rol"] = "Repartidor";
                $repartidor = new Repartidor($_SESSION["id"]);
                $repartidor -> consultar();
                $Datos= $repartidor -> getNombre(). " " . $repartidor -> getApellido();
                $log= new Log("","Repartidor: ".$Datos,$Accion,"Sin datos",date("Y-m-d"),date("H:i:s"));
                $log -> insertar();
                header("Location: index.php?pid=" . base64_encode("presentacion/sesionRepartidor.php"));
            }
}else{
   //header("Location: index.php?error=1");
}
?>