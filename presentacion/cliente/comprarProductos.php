<?php
$producto = new Producto();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$logActual = $producto -> consultarTodos();
$totalRegistros = $producto -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning ">
					<h4>Comprar productos</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($logActual) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Imagen</th>
							<th>Carrito</th>
						</tr>
						<?php 
						$i=1;
						foreach($logActual as $productoActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $productoActual -> getNombre() . "</td>";
						    echo "<td><div id='accion". $productoActual -> getIdProducto() . "'><p>". $productoActual -> getCantidad() . "</p>";
        						    ?>
                                    <script>
                                    $(document).ready(function(){
                                    	$("#cambiarEstado<?php echo $productoActual -> getIdProducto() ?>").click(function(e){
                                    		$('[data-toggle="tooltip"]').tooltip('hide');
                                    		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/cambiarIconoAjax.php")?>&idProducto=<?php echo $productoActual -> getIdProducto()?>&cantidad="+$("#carro<?php echo $productoActual->getIdProducto()?>").val();
                                    		$("#agregar<?php echo $productoActual -> getIdProducto() ?>").load(url);
                                    		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/restarCantidadAjax.php")?>&idProducto=<?php echo $productoActual -> getIdProducto()?>&cantidad="+$("#carro<?php echo $productoActual->getIdProducto()?>").val();
                                    		$("#accion<?php echo $productoActual -> getIdProducto() ?>").load(url);
                                    		
                                    	});
                                    });
           						    </script>
           						    
           						   
           						    
						<?php   						    
                            echo "</div></td>";
						    echo "<td>" . $productoActual -> getPrecio() . "</td>";
						    echo "<td>" . (($productoActual -> getImagen()!="")?"<img src='" . $productoActual -> getImagen() . "' height='80px'>":"") . "</td>";                  
						    echo "<td><div id='agregar". $productoActual -> getIdProducto()."'><input class='form-control-sm' type='number' name='carro".$productoActual->getIdProducto()."' id='carro".$productoActual->getIdProducto()."'  min='1' max='". $productoActual->getCantidad()."' value='1'><a id='cambiarEstado".$productoActual -> getIdProducto() ."' href='#' onclick='Funcion()'><span onclick='Funcion()' class='fas fa-cart-plus' data-toggle='tooltip' data-placement='left' title='Agregar al carrito'></span</a></div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					
					
				</div>
				<div class="card-footer text-muted">
                <a class="btn btn-warning" href="index.php?pid=<?php echo base64_encode("presentacion/cliente/mostrarCarrito.php")?>&idProductos=<?php print_r($arrayid) ?>&cantidad=<?php print_r($arraycant) ?>" role="button">Ir al carrito</a>          
              </div>
            </div>
		</div>
	</div>
</div>
 <script>
                                    function Funcion() {
                                    	<?php 
                                   		   array_push($arrayid,$productoActual->getIdProducto());
                                   		   array_push($arraycant,$productoActual->getCantidad());
                                   		   
                                      ?>
                                      
                                    }
                                    </script>
