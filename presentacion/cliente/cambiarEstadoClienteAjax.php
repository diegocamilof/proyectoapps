<?php
$repartidor = new Cliente($_GET["idCliente"], "", "", "", "", "", $_GET["nuevoEstado"]);
$repartidor -> consultar();
$datos=(($_GET["nuevoEstado"]==1)?"Habilito a ".$repartidor -> getNombre()." ".$repartidor -> getApellido():"Deshabilito a ".$repartidor -> getNombre()." ".$repartidor -> getApellido());
$administrador= new Administrador($_SESSION["id"]);
$administrador -> consultar();
$autor=$_SESSION["rol"].": ".$administrador -> getNombre()." ". $administrador -> getApellido();
$log =new Log("",$autor,"Cambio de estado",$datos,date("Y-m-d"),date("H:i:s"));
$log -> insertar();
$repartidor -> cambiarEstado();
echo ($_GET["nuevoEstado"]==1)?"<div id='icono" . $_GET["idCliente"] . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":"<div id='icono" . $_GET["idCliente"] . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>";

?>
