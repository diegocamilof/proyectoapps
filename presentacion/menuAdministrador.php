<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
$nombre = $administrador -> getNombre();
$apellido = $administrador -> getApellido();
$correo = $administrador -> getCorreo();
?>
<nav class="navbar navbar-expand-lg navbar-light bg-warning">
  <a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("presentacion/sesionAdministrador.php") ?>"><i
		class="fas fa-home"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Clientes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php  echo base64_encode("presentacion/cliente/consultarClientes.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php  echo base64_encode("presentacion/cliente/consultarClienteFiltro.php")?>"">Buscar</a>
        </div>
      </li>
   
    
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Productos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProductoFiltro.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/consultarProducto.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/producto/crearProducto.php")?>">Crear</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Repartidor
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/repartidor/consultarRepartidorFiltro.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/repartidor/consultarRepartidor.php")?>">Consultar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/repartidor/crearRepartidor.php")?>">Crear</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Log
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/log/buscarLogFiltro.php")?>">Buscar</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/log/consultarLog.php")?>">Consultar</a>
          
        </div>
      </li>
    </ul>
    <span class="navbar-text">
      Administrador:<?php echo $nombre!="" && $apellido!=""?" " . $nombre. " ". $apellido :  $correo ?>
    </span>
    <ul class="navbar-nav">
    <li class="nav-item active"><a class="nav-link"
				href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
    </ul>
  </div>
</nav>